# FP-Sanbercode-Reactjs-0820

Final Project
Membangun Web Frontend Reactjs
pada final project web frontend Reactjs ini anda diminta untuk membuat web app dengan syarat berikut ini:

Web App wajib menggunakan Material UI atau yang sejenisnya (Core UI, Ant Design)
wajib menggunakan repository baru untuk final project untuk namanya “FP-Sanbercode-Reactjs-0820”
wajib di deploy melalui netlify agar nantinya bisa digunakan
wajib ada function component, class component, state, hooks, context, react-router dom dan axios
Terdapat header, sidebar, footer dan content component
terdapat fitur login, register dan logout, dan ganti password
membuat halaman list movie dan review movie (detail movie dengan parameter id) dengan menampilkan gambar movie
membuat halaman list game dan detail game dengan menampilkan gambar game
membuat fitur delete data game dan data movie
membuat halaman data game dan list game
membuat halaman table game dan table movie
membuat filter berdasarkan minimal dari 3 field table dan search berdasarkan nama pada table game dan table movie
membuat fitur create dan edit movie atau game dengan route yang berbeda (misal url edit movie/edit/ID, untuk create urlnya movie/create)
membuat sort tiap field-field dalam tabel
jika ingin menambahkan fitur lain di persilahkan


===GAMES===

GET https://backendexample.sanbersy.com/api/data-game

GET https://backendexample.sanbersy.com/api/data-game/{ID_GAMES}

POST https://backendexample.sanbersy.com/api/data-game

PUT https://backendexample.sanbersy.com/api/data-game/{ID_GAMES}

DELETE https://backendexample.sanbersy.com/api/data-game/{ID_GAMES}

===MOVIES===

GET https://backendexample.sanbersy.com/api/data-movie

GET https://backendexample.sanbersy.com/api/data-movie/{ID_MOVIES}

POST https://backendexample.sanbersy.com/api/data-movie

PUT https://backendexample.sanbersy.com/api/data-movie/{ID_MOVIES}

DELETE https://backendexample.sanbersy.com/api/data-movie/{ID_MOVIES}

===USER===
POST https://backendexample.sanbersy.com/api/register
(parameter untuk register name, email dan password)

POST https://backendexample.sanbersy.com/api/user-login
(parameter untuk login email dan password)

POST https://backendexample.sanbersy.com/api/change-password
(parameter untuk change password current_password, new_password dan new_confirm_password)