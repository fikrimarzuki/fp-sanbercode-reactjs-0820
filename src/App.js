import React from 'react';
import { BrowserRouter as Router } from "react-router-dom";
import { Container, Grid, makeStyles } from "@material-ui/core";

import Header from "./components/Header";
import Content from "./pages/Content";
import Sidebar from "./components/Sidebar";
import Footer from "./components/Footer";

import AuthProvider from "./context/AuthProvider";

import './App.css';

const styling = makeStyles(theme => ({
  mainGrid: {
    marginTop: theme.spacing(2)
  }
}))

function App() {
  const classes = styling();

  return (
    <Router>
      <AuthProvider>
        <div className="App">
          <Container maxWidth="lg">
            <Header />
            <Grid container className={classes.mainGrid}>
              <Sidebar />
              <Content />
            </Grid>
          </Container>
          <Footer />
        </div>
      </AuthProvider>
    </Router>
  );
}

export default App;
