import React from "react";
import { Container, Typography, makeStyles } from "@material-ui/core";

const styling = makeStyles(theme => ({
  footer: {
    position: "fixed",
    backgroundColor: theme.palette.background.paper,
    padding: "15px",
    bottom: "0px",
    width: "100%",
    alignItems: "center",
    zIndex: 10000
  }
}))

export default function Footer() {
  const classes = styling();

  return (
    <footer className={classes.footer}>
      <Container maxWidth="lg">
        <Typography variant="body2" color="textSecondary" align="center">
          {'Copyright © Fikrimarzuki 2020.'}
        </Typography>
      </Container>
    </footer>
  )
}
