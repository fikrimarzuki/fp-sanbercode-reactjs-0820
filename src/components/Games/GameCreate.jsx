import React, { useState } from "react";
import { useHistory } from "react-router-dom";

import { GameContext } from "../../context/Index";

import { gamesActions } from "../../store";

import {
  Grid,
  Button,
  FormControl,
  TextField,
  FormControlLabel,
  Checkbox,
  makeStyles
} from "@material-ui/core";

const styling = makeStyles(theme => ({
  container: {
    
  },
  form: {
    display: "flex",
    justifyContent: "center",
    flexWrap: "wrap",
    "& .MuiFormControl-root": {
      margin: theme.spacing(3),
      width: "25ch",
      [theme.breakpoints.down("sm")]: {
        margin: "5px"
      }
    }
  },
  formGroup: {
    "& .MuiInputBase-root": {
      marginTop: "0px",
      height: "50px",
      width: "100%"
    },
    "& .MuiInputBase-input": {
      textAlign: "center"
    },
    "&.players": {
      display: "flex",
      flexDirection: "row",
      paddingLeft: "32px",
      [theme.breakpoints.down("sm")]: {
        paddingLeft: "15px"
      }
    }
  }
}))

export default function GameCreate() {
  const classes = styling();
  const history = useHistory();
  const { setGames } = React.useContext(GameContext);
  const [input, setInput] = useState();
  const [checked, setChecked] = useState({
    singlePlayer: false,
    multiplayer: false
  });

  const handleChange = prop => event => {
    setInput({ ...input, [prop]: event.target.value });
  }
  const handleChecked = event => {
    setChecked({ ...checked, [event.target.name]: event.target.checked });
  }

  const createGame = async event => {
    event.preventDefault();
    const payload = {
      ...input,
      ...checked
    }
    await gamesActions.createGame(payload);
    const { data } = await gamesActions.fetchGames();
    setGames(data);
    history.push("/games");
  }

  return (
    <div>
      <h1 style={{ textAlign: "center" }}>Create Game</h1>
      <Grid container item xs={12} md={11} className={classes.container} justify="center">
        <form className={classes.form} onSubmit={createGame}>
          <FormControl margin={"normal"} className={classes.formGroup}>
            <TextField
              required
              label="Name"
              onChange={handleChange("name")}
              placeholder="Game name"
              variant="outlined"
            />
          </FormControl>
          <FormControl margin={"normal"} className={classes.formGroup}>  
            <TextField
              required
              label="Genre"
              onChange={handleChange("genre")}
              variant="outlined"
            />
          </FormControl>
          <FormControl margin={"normal"} className={`${classes.formGroup} players`}>  
            <FormControlLabel
              control= {
                <Checkbox
                  color="primary"
                  checked={checked.singlePlayer}
                  onChange={handleChecked}
                  name="singlePlayer"
                />
              }
              label="Single Player"
            />
            <FormControlLabel
              control= {
                <Checkbox
                  color="primary"
                  checked={checked.multiplayer}
                  onChange={handleChecked}
                  name="multiplayer"
                />
              }
              label="Multiplayer"
              />
          </FormControl>
          <FormControl margin={"normal"} className={classes.formGroup}>
            <TextField
              required
              label="Platform"
              onChange={handleChange("platform")}
              variant="outlined"
            />
          </FormControl>
          <FormControl margin={"normal"} className={classes.formGroup}>
            <TextField
              required
              label="Release"
              type="number"
              onChange={handleChange("release")}
              variant="outlined"
            />
          </FormControl>
          <FormControl margin={"normal"} className={classes.formGroup}>
            <TextField
              required
              label="Image Url"
              onChange={handleChange("image_url")}
              variant="outlined"
            />
          </FormControl>
          <FormControl>
            <Button className={classes["btn-create"]} variant="contained" size="medium" color="primary" type="submit">Submit</Button>
          </FormControl>
        </form>
      </Grid>
    </div>
  )
}
