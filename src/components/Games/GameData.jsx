import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { GameContext } from "../../context/Index";

import {
  Paper,
  makeStyles
} from "@material-ui/core";

const styling = makeStyles(theme => ({
  paper: {
    width: "100%"
  },
  container: {
    maxHeight: 400
  },
  game: {
    display: "grid",
    gridTemplateColumns: "1fr 1fr",
    justifyContent: "space-around",
    alignItems: "center",
    paddingLeft: "20px"
  },
  link: {
    textDecoration: "none",
    color: "black",
    "&:hover": {
      color: "blue"
    }
  }
}))

export default function GameData() {
  const classes = styling();
  const { games } = useContext(GameContext);

  return (
    <div className={classes.paper}>
      <h1 style={{ textAlign: "center", marginBottom: "20px" }}>Game Data</h1>
      {
        games && games.length !== 0
          ? games.map(el => {
              return (
                <Paper key={el.id} className={classes.game}>
                  <div>
                    <Link to={`/games/${el.id}`}>
                      <h2>{el.name}</h2>
                    </Link>
                    <div>
                      <p>Genre: {el.genre}</p>
                      <p>Platform: {el.platform}</p>
                      {el.singlePlayer ? <p>Single Player</p> : ""} 
                      {el.multiplayer ? <p>Multiplayer</p> : ""}
                      <p>Release: {el.release}</p>
                    </div>
                  </div>
                  <img src={el.image_url} alt={el.name} style={{ maxWidth: "300px", objectFit: "cover" }}/>
                </Paper>
              )
            })
          : ""
      }
    </div>
  )
}
