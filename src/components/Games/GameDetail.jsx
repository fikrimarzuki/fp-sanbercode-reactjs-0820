import React, { useContext } from "react";
import { useParams } from "react-router-dom";
import { Paper } from "@material-ui/core";

import { GameContext } from "../../context/Index";

import { gamesActions } from "../../store";

export default function GameDetail() {
  const { id } = useParams();
  const { game, setGame } = useContext(GameContext);

  React.useEffect(() => {
    async function fetchGame() {
      const data = await gamesActions.fetchGame(id);
      setGame(data);
    }
    fetchGame()
  }, [id, setGame])

  return (
    <div>
      {
        game
          ? <Paper>
              <h2>{game.name}</h2>
              <div>
                <p>Genre: {game.genre}</p>
                <p>Platform: {game.platform}</p>
                {game.singlePlayer ? <p>Single Player</p> : ""} 
                {game.multiplayer ? <p>Multiplayer</p> : ""}
                <p>Release: {game.release}</p>
              </div>
              <img src={game.image_url} alt={game.name}/>
            </Paper>
          : ""
      }
    </div>
  )
}
