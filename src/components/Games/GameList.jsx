import React, { useContext } from "react";
import { Link } from "react-router-dom";

import { GameContext } from "../../context/Index";

import {
  Paper,
  Table,
  TableContainer,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  makeStyles,
} from "@material-ui/core";

const styling = makeStyles(theme => ({
  paper: {
    width: "100%"
  },
  container: {
    maxHeight: 500
  },
  link: {
    textDecoration: "none",
    color: "black",
    "&:hover": {
      color: "blue"
    }
  }
}))

export default function GameList() {
  const classes = styling();
  const { games } = useContext(GameContext);

  return (
    <Paper className={classes.paper}>
      <h1>Game List</h1>
      <TableContainer className={classes.container}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>No</TableCell>
              <TableCell>Name</TableCell>
              <TableCell>Genre</TableCell>
              <TableCell>Platform</TableCell>
              <TableCell>Player</TableCell>
              <TableCell>Image URL</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {
              games && games.length !== 0
                ? games.map((el, index) => {
                    return (
                      <TableRow key={el.id}>
                        <TableCell>{index + 1}</TableCell>
                        <TableCell>
                          <Link to={`/games/${el.id}`} className={classes.link}>
                            {el.name}
                          </Link>
                        </TableCell>
                        <TableCell>{el.genre}</TableCell> 
                        <TableCell>{el.platform}</TableCell> 
                        <TableCell>
                          {el.singlePlayer ? "single" : ""}
                          {el.multiplayer ? "multi" : ""}
                        </TableCell> 
                        <TableCell>{el.image_url && el.image_url.length > 20 ? `${el.image_url.substring(0, 21)}...` : el.image_url}</TableCell> 
                      </TableRow>
                    )
                  })
                : ""
            }
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  )
}
