import React, { useContext, useState } from "react";
import { Link, useHistory } from "react-router-dom";

import { GameContext } from "../../context/Index";

import { gamesActions } from "../../store";

import {
  Paper,
  Table,
  TableContainer,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TableSortLabel,
  Select,
  MenuItem,
  Input,
  makeStyles,
} from "@material-ui/core";
import {
  Edit as EditIcon,
  Delete as DeleteIcon
} from "@material-ui/icons"

const styling = makeStyles(theme => ({
  paper: {
    width: "100%"
  },
  container: {
    maxHeight: 400
  },
  link: {
    textDecoration: "none",
    color: "black",
    "&:hover": {
      color: "blue"
    }
  },
  buttonEdit: {
    textDecoration: "none",
    color: "black",
    "&:hover": {
      color: "yellow"
    }
  },
  buttonDelete: {
    cursor: "pointer",
    "&:hover": {
      color: "red"
    }
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  row: {
    "&:hover": {
      backgroundColor: "#f1f1f1",
      cursor: "pointer"
    }
  }
}))

export default function MovieTable() {
  const classes = styling();
  const history = useHistory();
  const { games, setGames } = useContext(GameContext);
  const [data, setData] = useState([]);
  const [keyword, setKeyword] = useState("name");
  const [order, setOrder] = useState("asc");
  const [orderBy, setOrderBy] = useState("name");
  const [tableHead] = useState([
    { id: 1, key: 'name', label: 'Name' },
    { id: 2, key: 'genre', label: 'Genre' },
    { id: 3, key: 'singlePlayer', label: 'Single Player' },
    { id: 4, key: 'multiplayer', label: 'Multiplayer' },
    { id: 5, key: 'platform', label: 'Platform' },
    { id: 6, key: 'release', label: 'Release' }
  ])

  React.useEffect(() => {
    setData(games);
  }, [games])

  const createSortHandler = property => event =>{
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  }

  const descendingComparator = (a, b, orderBy) => {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
  }

  const getComparator = (order, orderBy) => {
    return order === 'desc'
      ? (a, b) => descendingComparator(a, b, orderBy)
      : (a, b) => -descendingComparator(a, b, orderBy);
  }

  const stableSort = (array, comparator) => {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) return order;
      return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
  }

  const deleteGame = async (event, id) => {
    event.stopPropagation();
    await gamesActions.deleteGame(id);
    const gameData = await gamesActions.fetchGames();
    setGames(gameData);
    setData(gameData);
  }

  const handleChange = event => {
    setKeyword(event.target.value);
  }

  const handleChangeSearch = event => {
    const value = event.target.value;
    if (value) {
      const filterData = games.filter(el => {
        if (typeof el[keyword] === "string") {
          return el[keyword].toLowerCase().includes(value.toLowerCase());
        } else {
          return el[keyword].toString().includes(value);
        }
      })
      setData(filterData);
    } else {
      setData(games);
    }
  }

  return (
    <Paper className={classes.paper}>
      <div style={{ textAlign: "center", marginBottom: "10px" }}>
        <h1 style={{ marginBottom: "10px" }}>Game Table</h1>
        <Select
          id="keyword-type"
          value={keyword}
          onChange={handleChange}
        >
          <MenuItem value={"name"}>Name</MenuItem>
          <MenuItem value={"genre"}>Genre</MenuItem>
          <MenuItem value={"platform"}>Platform</MenuItem>
          <MenuItem value={"release"}>Release</MenuItem>
        </Select>
        <Input placeholder="search" onChange={handleChangeSearch}/>
      </div>
      <TableContainer className={classes.container}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Actions</TableCell>
              <TableCell>No</TableCell>
              {
                tableHead.map(headCell => {
                  return (
                    <TableCell
                      key={headCell.id}
                      sortDirection={orderBy === headCell.key ? order : false}
                    >
                      <TableSortLabel
                        active={orderBy === headCell.key}
                        direction={orderBy === headCell.key ? order : "asc"}
                        onClick={createSortHandler(headCell.key)}
                      >
                        {headCell.label}
                        {orderBy === headCell.key ? (
                          <span className={classes.visuallyHidden}>
                            {order === "desc" ? "sorted descending" : "sorted ascending"}
                          </span>
                        ) : null}
                      </TableSortLabel>  
                    </TableCell>
                  )
                })
              }
              <TableCell>Image URL</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {
              data && data.length !== 0 
                ? stableSort(data, getComparator(order, orderBy))
                    .map((el, index) => {
                      return (
                        <TableRow key={el.id} className={classes.row} onClick={() => history.push(`/games/${el.id}`)}>
                          <TableCell>
                            <Link to={`/games/edit/${el.id}`} onClick={e => e.stopPropagation()} className={classes.buttonEdit}>
                              <EditIcon />
                            </Link>
                            <DeleteIcon onClick={event => deleteGame(event, el.id)} className={classes.buttonDelete}/>
                          </TableCell>
                          <TableCell>{index + 1}</TableCell>
                          <TableCell>
                            <Link to={`/games/${el.id}`} className={classes.link}>
                              {el.name && el.name.length > 20 ? `${el.name.substring(0, 20)}...` : el.name}
                            </Link>
                          </TableCell>
                          <TableCell>{el.genre}</TableCell> 
                          <TableCell>{el.singlePlayer ? "yes" : "no"}</TableCell> 
                          <TableCell>{el.multiplayer ? "yes" : "no"}</TableCell> 
                          <TableCell>{el.platform}</TableCell> 
                          <TableCell>{el.release}</TableCell> 
                          <TableCell>{el.image_url && el.image_url.length > 20 ? `${el.image_url.substring(0, 21)}...` : el.image_url}</TableCell> 
                        </TableRow>
                      )
                    })
                : <TableRow></TableRow>
            }
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  )
}
