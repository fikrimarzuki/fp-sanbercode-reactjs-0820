import React, { useState, useContext } from "react";
import { useHistory, useParams } from "react-router-dom";

import { GameContext } from "../../context/Index";
import { gamesActions } from "../../store";

import {
  Button,
  FormControl,
  TextField,
  FormControlLabel,
  Checkbox,
  makeStyles
} from "@material-ui/core";

const styling = makeStyles(theme => ({
  form: {}
}))

export default function GameUpdate() {
  const classes = styling();
  const history = useHistory();
  const { id } = useParams();
  const [input, setInput] = useState({});
  const [checked, setChecked] = useState({
    singlePlayer: false,
    multiplayer: false
  });
  const { setGame, setGames } = useContext(GameContext);
 
  React.useEffect(() => {
    async function fetchGame() {
      const data = await gamesActions.fetchGame(id)
      setGame(data);
      setInput(data);
      setChecked({
        singlePlayer: data.singlePlayer ? true : false,
        multiplayer: data.multiplayer ? true : false
      })
    }
    fetchGame()
  }, [id, setGame])

  const handleChange = prop => event => {
    setInput({ ...input, [prop]: event.target.value });
  }
  const handleChecked = event => {
    setChecked({ ...checked, [event.target.name]: event.target.checked });
  }

  const updateGame = async event => {
    event.preventDefault();
    await gamesActions.updateGame({ ...input, ...checked, id })
    const data = await gamesActions.fetchGames();
    setGames(data);
    history.push("/games");
  }

  return (
    <div>
      <h1 style={{ textAlign: "center" }}>Update Game</h1>
      <form className={classes.form} onSubmit={updateGame}>
        <FormControl>
          <TextField
            required
            label="Name"
            onChange={handleChange("name")}
            value={input.name ? input.name : ""}
            variant="outlined"
          />
          <TextField
            required
            label="Genre"
            onChange={handleChange("genre")}
            value={input.genre ? input.genre : ""}
            variant="outlined"
          />
          <TextField
            required
            label="Platform"
            onChange={handleChange("platform")}
            value={input.platform ? input.platform : ""}
            variant="outlined"
          />
          <FormControlLabel
            control= {
              <Checkbox
                color="primary"
                checked={checked.singlePlayer ? true : false}
                onChange={handleChecked}
                name="singlePlayer"
              />
            }
            label="Single Player"
          />
          <FormControlLabel
            control= {
              <Checkbox
                color="primary"
                checked={checked.multiplayer ? true : false}
                onChange={handleChecked}
                name="multiplayer"
              />
            }
            label="Multiplayer"
          />
          <TextField
            required
            label="Release"
            type="number"
            onChange={handleChange("release")}
            value={input.release ? input.release : ""}
            variant="outlined"
          />
          <TextField
            required
            label="Image Url"
            onChange={handleChange("image_url")}
            value={input.image_url ? input.image_url : ""}
            variant="outlined"
          />
          <Button className={classes["btn-update"]} variant="contained" size="medium" color="primary" type="submit">Update</Button>
        </FormControl>
      </form>
    </div>
  )
}
