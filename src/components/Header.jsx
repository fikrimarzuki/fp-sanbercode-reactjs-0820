import React from "react";
import { Link, useHistory } from "react-router-dom";
import Cookies from "js-cookie";

import { AuthContext } from "../context/Index";

import {
  Button,
  Toolbar,
  makeStyles,
  Link as Navigation
} from "@material-ui/core";

const styling = makeStyles(theme => ({
  navbar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
    justifyContent: "space-between"
  },
  link: {
    textDecoration: "none",
    cursor: "pointer",
    "&:hover": {
      textDecoration: "none"
    }
  }
}))

export default function Header() {
  const classes = styling();
  const history = useHistory();
  const { user, setUser } = React.useContext(AuthContext);

  const logout = () => {
    setUser(null);
    Cookies.remove("token");
    history.replace("/login");
  }

  return (
    <>
      <Toolbar className={`${classes.navbar} navbar`}>
        <Link to="/">
          <Button size="medium">FINAL PROJECT</Button>
        </Link>
        <Link to="/">Home</Link>        
        <Link to="/games">Games</Link>
        <Link to="/movies">Movies</Link>
        {
          user
            ? <>
              <Link to="/change-pass">Change password</Link>
              <Navigation onClick={logout} className={classes.link}>Logout</Navigation>
            </>
            : <>
              <Link to="/login">Login</Link>
              <Link to="/register">Register</Link>
            </>
        }
      </Toolbar>
    </>
  )
}
