import React, { useState } from "react";
import { useHistory } from "react-router-dom";

import { MovieContext } from "../../context/Index";

import { moviesActions } from "../../store";

import {
  Button,
  FormControl,
  Input,
  FormLabel,
  makeStyles
} from "@material-ui/core";

const styling = makeStyles(theme => ({
  form: {
    display: "flex",
    justifyContent: "center",
    flexWrap: "wrap",
    "& .MuiFormControl-root": {
      margin: theme.spacing(5),
      width: "25ch",
    }
  },
  formGroup: {
    "& .MuiFormLabel-root": {
      textAlign: "center"
    },
    "& .MuiInputBase-root": {
      marginTop: "0px"
    },
    "& .MuiInputBase-input": {
      textAlign: "center"
    },
    "&.duration": {
      width: "11ch"
    },
    "&.year": {
      width: "9ch"
    },
    "&.rating": {
      width: "9ch"
    }
  }
}))

export default function MovieCreate() {
  const classes = styling();
  const history = useHistory();
  const { setMovies } = React.useContext(MovieContext);
  const [input, setInput] = useState({});

  const handleChange = prop => event => {
    setInput({ ...input, [prop]: event.target.value });
  }

  const createMovie = async event => {
    event.preventDefault();
    await moviesActions.createMovie(input);
    const data = await moviesActions.fetchMovies();
    setMovies(data);
    history.push("/movies");
  }

  return (
    <div>
      <h1 style={{ textAlign: "center" }}>Create Movie</h1>
      <form className={classes.form} onSubmit={createMovie}>
        <FormControl margin={"normal"} className={classes.formGroup}>
          <FormLabel>Title</FormLabel>
          <Input
            required
            onChange={handleChange("title")}
          />
        </FormControl>
        <FormControl margin={"normal"} className={`${classes.formGroup} duration`}>
          <FormLabel>Duration</FormLabel>
          <Input
            required
            type="number"
            onChange={handleChange("duration")}
          />
        </FormControl>
        <FormControl margin={"normal"} className={`${classes.formGroup} year`}>
          <FormLabel>Year</FormLabel>
          <Input
            required
            type="number"
            onChange={handleChange("year")}
          />
        </FormControl>
        <FormControl margin={"normal"} className={`${classes.formGroup} rating`}>
          <FormLabel>Rating</FormLabel>
          <Input
            required
            type="number"
            onChange={handleChange("rating")}
          />
        </FormControl>
        <FormControl margin={"normal"} className={classes.formGroup}>
          <FormLabel>Genre</FormLabel>
          <Input
            required
            onChange={handleChange("genre")}
          />
        </FormControl>
        <FormControl margin={"normal"} className={classes.formGroup}>
          <FormLabel>Review</FormLabel>
          <Input
            required
            onChange={handleChange("review")}
          />
        </FormControl>
        <FormControl margin={"normal"} className={classes.formGroup}>
          <FormLabel>Image URL</FormLabel>
          <Input
            required
            onChange={handleChange("image_url")}
          />
        </FormControl>
        <FormControl>
          <Button className={classes["btn-create"]} variant="contained" size="medium" color="primary" type="submit">Submit</Button>
        </FormControl>
      </form>
    </div>
  )
}
