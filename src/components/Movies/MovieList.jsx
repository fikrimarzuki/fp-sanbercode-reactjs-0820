import React, { useContext } from "react";
import { Link } from "react-router-dom";

import { MovieContext } from "../../context/Index";

import {
  Paper,
  makeStyles,
} from "@material-ui/core";

const styling = makeStyles(theme => ({
  paper: {
    width: "100%"
  },
  container: {
    maxHeight: 400
  },
  movie: {
    display: "grid",
    gridTemplateColumns: "1fr 1fr",
    justifyContent: "space-around",
    alignItems: "center",
    paddingLeft: "20px"
  },
  link: {
    textDecoration: "none",
    color: "black",
    "&:hover": {
      color: "blue"
    }
  }
}))

export default function MovieList() {
  const classes = styling();
  const { movies } = useContext(MovieContext);

  return (
    <div className={classes.paper}>
      <h1 style={{ textAlign: "center", marginBottom: "20px" }}>Movie List</h1>
      {
        movies && movies.length !== 0 
          ? movies.map(el => {
              return (
                <Paper key={el.id} className={classes.movie}>
                  <div>
                    <Link to={`/movies/${el.id}`}>
                      <h2>{el.title}</h2>
                    </Link>
                    <div>
                      <p>Genre: {el.genre}</p>
                      <p>Rating: {el.rating}</p>
                      <p>{el.year}</p>
                      <p>{el.duration}</p>
                      <p>{el.review}</p>
                    </div>
                  </div>
                  <img src={el.image_url} alt={el.name} style={{ maxWidth: "300px", objectFit: "cover" }}/>
                </Paper>
              )
            })
          : ""
      }
    </div>
  )
}
