import React, { useContext } from "react";
import { useParams } from "react-router-dom";
import { Paper, makeStyles } from "@material-ui/core";

import { MovieContext } from "../../context/Index";

import { moviesActions } from "../../store";

const styling = makeStyles(theme => ({
  paper: {
    width: "100%"
  },
  container: {},
  link: {
    textDecoration: "none",
    color: "black",
    "&:hover": {
      color: "blue"
    }
  }
}))

export default function MovieReview() {
  const { id } = useParams();
  const { movie, setMovie } = useContext(MovieContext);
  const classes = styling();

  React.useEffect(() => {
    async function fetchMovie() {
      const data = await moviesActions.fetchMovie(id);
      setMovie(data);
    }
    fetchMovie()
  }, [id, setMovie])

  return (
    <div className={classes.container}>
      {
        movie
          ? <Paper>
              <h2>{movie.title}</h2>
              <div>
                <p>Genre: {movie.genre}</p>
                <p>Platform: {movie.rating}</p>
                <p>{movie.year}</p>
                <p>{movie.duration}</p>
                <p>{movie.review}</p>
              </div>
              <img src={movie.image_url} alt={movie.name}/>
            </Paper>
          : ""
      }
    </div>
  )
}
