import React, { useContext, useState } from "react";
import { Link, useHistory } from "react-router-dom";

import { MovieContext } from "../../context/Index";

import { moviesActions } from "../../store";

import {
  Paper,
  Table,
  TableContainer,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TableSortLabel,
  Select,
  MenuItem,
  Input,
  makeStyles,
} from "@material-ui/core";
import {
  Edit as EditIcon,
  Delete as DeleteIcon
} from "@material-ui/icons"

const styling = makeStyles(theme => ({
  paper: {
    width: "100%"
  },
  container: {
    maxHeight: 400
  },
  link: {
    textDecoration: "none",
    color: "black",
    "&:hover": {
      color: "blue"
    }
  },
  buttonEdit: {
    textDecoration: "none",
    color: "black",
    "&:hover": {
      color: "yellow"
    }
  },
  buttonDelete: {
    cursor: "pointer",
    "&:hover": {
      color: "red"
    }
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  row: {
    "&:hover": {
      backgroundColor: "#f1f1f1",
      cursor: "pointer"
    }
  }
}))

export default function MovieTable() {
  const classes = styling();
  const history = useHistory();
  const { movies, setMovies } = useContext(MovieContext);
  const [data, setData] = useState([]);
  const [keyword, setKeyword] = useState("title");
  const [order, setOrder] = useState("asc");
  const [orderBy, setOrderBy] = useState("title");
  const [tableHead] = useState([
    { id: 1, key: "title", label: "Title" },
    { id: 2, key: "description", label: "Description" },
    { id: 3, key: "genre", label: "Genre" },
    { id: 4, key: "rating", label: "Rating" },
    { id: 5, key: "year", label: "Year" },
    { id: 6, key: "duration", label: "Duration" },
    { id: 7, key: "review", label: "Review" }
  ])

  React.useEffect(() => {
    setData(movies);
  }, [movies])

  const createSortHandler = property => event =>{
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  }

  const descendingComparator = (a, b, orderBy) => {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
  }

  const getComparator = (order, orderBy) => {
    return order === 'desc'
      ? (a, b) => descendingComparator(a, b, orderBy)
      : (a, b) => -descendingComparator(a, b, orderBy);
  }

  const stableSort = (array, comparator) => {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) return order;
      return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
  }

  const deleteMovie = async (event, id) => {
    event.stopPropagation();
    await moviesActions.deleteMovie(id);
    const movieData = await moviesActions.fetchMovies();
    setMovies(movieData);
    setData(movieData);
  }

  const handleChange = event => {
    setKeyword(event.target.value);
  }

  const handleChangeSearch = event => {
    const value = event.target.value;
    if (value) {
      const filterData = movies.filter(el => {
        if (typeof el[keyword] === "string") {
          return el[keyword].toLowerCase().includes(value.toLowerCase());
        } else {
          return el[keyword].toString().includes(value);
        }
      })
      setData(filterData);
    } else {
      setData(movies);
    }
  }

  return (
    <Paper className={classes.paper}>
      <div style={{ textAlign: "center", marginBottom: "10px" }}>
        <h1 style={{ marginBottom: "10px" }}>Movie Table</h1>
        <Select
          id="keyword-type"
          value={keyword}
          onChange={handleChange}
        >
          <MenuItem value={"title"}>Title</MenuItem>
          <MenuItem value={"genre"}>Genre</MenuItem>
          <MenuItem value={"rating"}>Rating</MenuItem>
          <MenuItem value={"year"}>Year</MenuItem>
        </Select>
        <Input placeholder="search" onChange={handleChangeSearch}/>
      </div>
      <TableContainer className={classes.container}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Actions</TableCell>
              <TableCell>No</TableCell>
              {
                tableHead.map(headCell => {
                  return (
                    <TableCell
                      key={headCell.id}
                      sortDirection={orderBy === headCell.key ? order : false}
                    >
                      <TableSortLabel
                        active={orderBy === headCell.key}
                        direction={orderBy === headCell.key ? order : "asc"}
                        onClick={createSortHandler(headCell.key)}
                      >
                        {headCell.label}
                        {orderBy === headCell.key ? (
                          <span className={classes.visuallyHidden}>
                            {order === "desc" ? "sorted descending" : "sorted ascending"}
                          </span>
                        ) : null}
                      </TableSortLabel>  
                    </TableCell>
                  )
                })
              }
              <TableCell>Image URL</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {
              data && data.length !== 0 
                ? stableSort(data, getComparator(order, orderBy))
                    .map((el, index) => {
                      return (
                        <TableRow key={el.id} className={classes.row} onClick={() => history.push(`/movies/${el.id}`)}>
                          <TableCell>
                            <Link to={`/movies/edit/${el.id}`} onClick={e => e.stopPropagation()} className={classes.buttonEdit}>
                              <EditIcon />
                            </Link>
                            <DeleteIcon onClick={event => deleteMovie(event, el.id)} className={classes.buttonDelete}/>
                          </TableCell>
                          <TableCell>{index + 1}</TableCell>
                          <TableCell>
                            <Link to={`/movies/${el.id}`} className={classes.link}>
                              {el.title && el.title.length > 20 ? `${el.title.substring(0, 20)}...` : el.title}
                            </Link>
                          </TableCell>
                          <TableCell>{el.description && el.description.length > 20 ? `${el.description.substring(0, 20)}...` : el.description}</TableCell>
                          <TableCell>{el.genre}</TableCell> 
                          <TableCell>{el.rating}</TableCell> 
                          <TableCell>{el.year}</TableCell> 
                          <TableCell>{el.duration}</TableCell> 
                          <TableCell>{el.review}</TableCell> 
                          <TableCell>{el.image_url && el.image_url.length > 20 ? `${el.image_url.substring(0, 21)}...` : el.image_url}</TableCell> 
                        </TableRow>
                      )
                    })
                : <TableRow></TableRow>
            }
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  )
}
