import React, { useState, useContext } from "react";
import { useHistory, useParams } from "react-router-dom";

import { MovieContext } from "../../context/Index";
import { moviesActions } from "../../store";

import {
  Button,
  FormControl,
  FormLabel,
  Input,
  makeStyles
} from "@material-ui/core";

const styling = makeStyles(theme => ({
  form: {
    display: "flex",
    justifyContent: "center",
    flexWrap: "wrap",
    "& .MuiFormControl-root": {
      margin: theme.spacing(5),
      width: "25ch",
    }
  },
  formGroup: {
    "& .MuiFormLabel-root": {
      textAlign: "center"
    },
    "& .MuiInputBase-root": {
      marginTop: "0px"
    },
    "& .MuiInputBase-input": {
      textAlign: "center"
    },
    "&.duration": {
      width: "11ch"
    },
    "&.year": {
      width: "9ch"
    },
    "&.rating": {
      width: "9ch"
    }
  }
}))

export default function MovieUpdate() {
  const classes = styling();
  const history = useHistory();
  const { id } = useParams();
  const [input, setInput] = useState({});
  const { setMovie, setMovies } = useContext(MovieContext);

  React.useEffect(() => {
    async function fetchMovie() {
      const data = await moviesActions.fetchMovie(id);
      setMovie(data);
      setInput(data);
    }
    fetchMovie()
  }, [id, setMovie])


  const handleChange = prop => event => {
    setInput({ ...input, [prop]: event.target.value });
  }

  const updateMovie = async event => {
    event.preventDefault();
    await moviesActions.updateMovie({ ...input, id })
    const data = await moviesActions.fetchMovies();
    setMovies(data);
    history.push("/movies");
  }

  return (
    <div>
      <h1 style={{ textAlign: "center" }}>Update Movie</h1>
      <form className={classes.form} onSubmit={updateMovie}>
        <FormControl margin={"normal"} className={classes.formGroup}>
          <FormLabel>Title</FormLabel>
          <Input
            required
            value={input.title ? input.title : ""}
            onChange={handleChange("title")}
          />
        </FormControl>
        <FormControl margin={"normal"} className={`${classes.formGroup} duration`}>
          <FormLabel>Duration</FormLabel>
          <Input
            required
            type="number"
            value={input.duration ? input.duration : 120}
            onChange={handleChange("duration")}
          />
        </FormControl>
        <FormControl margin={"normal"} className={`${classes.formGroup} year`}>
          <FormLabel>Year</FormLabel>
          <Input
            required
            type="number"
            value={input.year ? input.year : 2020}
            onChange={handleChange("year")}
          />
        </FormControl>
        <FormControl margin={"normal"} className={`${classes.formGroup} rating`}>
          <FormLabel>Rating</FormLabel>
          <Input
            required
            type="number"
            value={input.rating ? input.rating : 0}
            onChange={handleChange("rating")}
          />
        </FormControl>
        <FormControl margin={"normal"} className={classes.formGroup}>
          <FormLabel>Genre</FormLabel>
          <Input
            required
            value={input.genre ? input.genre : ""}
            onChange={handleChange("genre")}
          />
        </FormControl>
        <FormControl margin={"normal"} className={classes.formGroup}>
          <FormLabel>Review</FormLabel>
          <Input
            required
            value={input.review ? input.review : ""}
            onChange={handleChange("review")}
          />
        </FormControl>
        <FormControl margin={"normal"} className={classes.formGroup}>
          <FormLabel>Image URL</FormLabel>
          <Input
            required
            value={input.image_url ? input.image_url : ""}
            onChange={handleChange("image_url")}
          />
        </FormControl>
        <FormControl>
          <Button className={classes["btn-update"]} variant="contained" size="medium" color="primary" type="submit">Update</Button>
        </FormControl>
      </form>
    </div>
  )
}
