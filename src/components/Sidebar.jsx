import React, { useState, useEffect } from "react";
import { useLocation, useHistory } from "react-router-dom"
import { Grid, Paper, Link, makeStyles } from "@material-ui/core";
import { AuthContext } from "../context/Index";

const styling = makeStyles(theme => ({
  container: {
    marginBottom: "60px",
    [theme.breakpoints.down("sm")]: {
      marginBottom: "20px"
    }
  },
  paper: {
    height: "100%",
    paddingTop: "20px",
    [theme.breakpoints.down("sm")]: {
      padding: "10px"
    }
  },
  mainSidebar: {
    width: "100%",
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    "& div": {
      marginBottom: "10px"
    },
    "& a": {
      color: "black",
      fontWeight: "bold"
    },
    "& a:hover": {
      textDecoration: "none",
      cursor: "pointer",
      color: "blue"
    },
    [theme.breakpoints.down("sm")]: {
      display: "flex",
      justifyContent: "space-around"
    }
  }
}))

export default function Sidebar() {
  const location = useLocation();
  const history = useHistory();
  const classes = styling();
  const { user } = React.useContext(AuthContext);

  const [sidebarMenu, setSidebarMenu] = useState([]);
  const [sidebarGame] = useState([
    { id: 1, name: "Data", link: "/games", isAuth: false },
    { id: 2, name: "List", link: "/games/list", isAuth: false },
    { id: 3, name: "Create", link: "/games/create", isAuth: true} ,
    { id: 4, name: "Table", link: "/games/table", isAuth: true }
  ])
  const [sidebarMovie] = useState([
    { id: 1, name: "List", link: "/movies/list", isAuth: false },
    { id: 2, name: "Create", link: "/movies/create", isAuth: true },
    { id: 3, name: "Table", link: "/movies/table", isAuth: true }
  ])

  useEffect(() => {
    if (location.pathname.includes("/games")) {
      setSidebarMenu(sidebarGame);
    } else if (location.pathname.includes("/movies")) {
      setSidebarMenu(sidebarMovie);
    } else {
      setSidebarMenu([])
    }
  }, [location.pathname, sidebarGame, sidebarMovie]);

  return (
    <Grid item xs={12} s={1} md={1} className={classes.container}>
      {
        sidebarMenu.length === 0
          ? ""
          :  <Paper className={classes.paper}>
              <div className={classes.mainSidebar}>
                  {
                    sidebarMenu && sidebarMenu.length !== 0
                      ? sidebarMenu.map(el => {
                        return (
                          <div key={el.id}>
                            {
                              (el.isAuth && user) || (!el.isAuth)
                                ? <Link onClick={() => history.push(el.link)}>
                                    {el.name}
                                  </Link>
                                : ""
                            }
                          </div>
                        )
                      })
                      : ""
                  }
              </div>
            </Paper>
      }
    </Grid>
  )
}
