import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { usersActions } from "../../store/";

import {
  Grid,
  Button,
  FormControl,
  TextField,
  makeStyles
} from '@material-ui/core';

const styling = makeStyles(theme => ({
  container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  form: {
    "& .MuiFormControl-root .MuiButton-root": {
      marginTop: "25px"
    }
  }
}))

export default function ChangePassword() {
  const classes = styling();
  const history = useHistory();
  const [input, setInput] = useState({})
  const [errors, setErrors] = useState({
    msg: "",
    showError: false
  })

  const handleChange = prop => event => {
    setInput({ ...input, [prop]: event.target.value });
  }

  const changePassword = async event => {
    event.preventDefault();
    const data = await usersActions.changePass(input);
    if (!data || data.error) {
      setInput({ ...input })
      setErrors({ msg: data.error, showError: true });
    } else {
      setInput({ current_password: "", new_password: "", new_password_confirm: "" })
      setErrors({ msg: "", showError: false });
      history.push("/");
    }
  }

  return (
    <Grid
        container
        item
        xs={12}
        direction="column"
        alignItems="center"
        justify="center"
      >
        <div className={classes.container}>
          <div style={{ textAlign: "center", marginBottom: "25px" }}>
            <h1>Change Password</h1>
            <p style={{ color: "red" }}>
              { errors.showError ? `${errors.msg} check your input` : "" }
            </p>
          </div>
          <form className={classes.form} onSubmit={changePassword}>
            <FormControl>
              <TextField
                error={false}
                required
                label="Current Password"
                type="password"
                onChange={handleChange("current_password")}
                helperText={"Write your old password here"}
                variant="outlined"
              />
              <TextField
                error={false}
                required
                label="New Password"
                type="password"
                onChange={handleChange("new_password")}
                helperText={"Write your new password here"}
                variant="outlined"
              />
              <TextField
                error={false}
                required
                label="Confirm Password"
                onChange={handleChange("new_confirm_password")}
                type="password"
                helperText={"Confirm your new password here"}
                variant="outlined"
              />
              <Button className={classes["btn-change-pass"]} variant="contained" size="medium" color="primary" type="submit">Change Password</Button>
            </FormControl>
          </form>
        </div>
      </Grid>
  )
}
