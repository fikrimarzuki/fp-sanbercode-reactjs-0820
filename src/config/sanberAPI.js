const axios = require("axios");

module.exports = axios.create({
  baseURL: "https://backendexample.sanbersy.com/api"
});
