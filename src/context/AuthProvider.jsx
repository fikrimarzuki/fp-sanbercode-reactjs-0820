import React from "react";
import Cookie from "js-cookie";
import { AuthContext } from "./Index";

export default function AuthProvider(props) {
  const currentUser = Cookie.get("token");
  const initiateUser = currentUser ? currentUser : null;
  const [user, setUser] = React.useState(initiateUser);

  return (
    <AuthContext.Provider value={{ user, setUser }}>
      {props.children}
    </AuthContext.Provider>
  )
}