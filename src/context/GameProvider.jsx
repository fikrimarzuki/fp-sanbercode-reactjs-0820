import React from "react";
import { GameContext } from "./Index";

export default function GameProvider(props) {
  const [games, setGames] = React.useState([]);
  const [game, setGame] = React.useState([]);

  return (
    <GameContext.Provider value={{games, game, setGame, setGames}}>
      {props.children}
    </GameContext.Provider>
  )
}