import React from "react";

export const Context = React.createContext();
export const AuthContext = React.createContext();
export const MovieContext = React.createContext();
export const GameContext = React.createContext();
