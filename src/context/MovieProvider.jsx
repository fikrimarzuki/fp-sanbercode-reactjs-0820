import React, { useState } from "react";
import { MovieContext } from "./Index";

export default function MovieProvider(props) {
  const [movies, setMovies] = useState([]);
  const [movie, setMovie] = useState([]);
  return (
    <MovieContext.Provider value={{ movies, movie, setMovies, setMovie }}>
      {props.children}
    </MovieContext.Provider>
  )
}