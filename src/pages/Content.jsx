import React from "react";
import { Route, Switch } from "react-router-dom";
import { Grid, makeStyles } from "@material-ui/core";

import Home from "./LandingPage";
import Login from "./Users/Login";
import Register from "./Users/Register";
import ChangePass from "../components/Users/ChangePassword";
import Games from "./Games/Index";
import Movies from "./Movies/Index";
import PageNotFound from "./PageNotFound";

import GameProvider from "../context/GameProvider";
import MovieProvider from "../context/MovieProvider";

const styling = makeStyles(theme => ({
  container: {
    marginBottom: "60px",
    paddingLeft: "20px"
  }
}))

export default function Content() {
  const classes = styling();

  return (
    <>
      <Grid
        container
        item
        xs={12}
        s={11}
        md={11}
        justify="center"
        direction="row"
        alignItems="center"
        className={classes.container}
      >
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route exact path="/home">
            <Home />
          </Route>
          <Route exact path="/login">
            <Login />
          </Route>
          <Route exact path="/register">
            <Register />
          </Route>
          <Route exact path="/change-pass">
            <ChangePass />
          </Route>
          <Route path="/games">
            <GameProvider>
              <Games />
            </GameProvider>
          </Route>
          <Route path="/movies">
            <MovieProvider>
              <Movies />
            </MovieProvider>
          </Route>
          <Route path="*">
            <PageNotFound />
          </Route>
        </Switch>
      </Grid>
    </>
  )
}
