import React, { useEffect } from "react";
import { Switch, Route, useRouteMatch } from "react-router-dom";
import { gamesActions } from "../../store";

import GameList from "../../components/Games/GameList";
import GameTable from "../../components/Games/GameTable";
import GameCreate from "../../components/Games/GameCreate";
import GameUpdate from "../../components/Games/GameUpdate";
import GameDetail from "../../components/Games/GameDetail";
import GameData from "../../components/Games/GameData";

import { GameContext } from "../../context/Index";

export default function Games() {
  let { path } = useRouteMatch();
  const { setGames } = React.useContext(GameContext)

  useEffect(() => {
    async function fetchData() {
      const data = await gamesActions.fetchGames();
      setGames(data);
    }
    fetchData();
  }, [setGames])

  return (
    <div style={{ width: "100%" }}>
      <Switch>
        <Route exact path={path}>
          <GameData />
        </Route>
        <Route path={`${path}/create`}>
          <GameCreate />
        </Route>
        <Route path={`${path}/list`}>
          <GameList />
        </Route>
        <Route path={`${path}/table`}>
          <GameTable />
        </Route>
        <Route path={`${path}/edit/:id`}>
          <GameUpdate />
        </Route>
        <Route path={`${path}/:id`}>
          <GameDetail />
        </Route>
      </Switch>
    </div>
  )
}
