import React from "react";
import { Link } from "react-router-dom";

import { Paper, Grid, makeStyles } from "@material-ui/core";

const styling = makeStyles(theme => ({
  img: {
    borderRadius: "10px",
    maxWidth: "100%",
    height: "350px",
    objectFit: "cover",
    "&:hover": {
      transform: "scale(0.8)",
      transition: "0.5s",
      cursor: "pointer",
      objectFit: "contain"
    }
  }
}))

export default function LandingPage() {
  const classes = styling();

  return (
    <div>
      <Grid container spacing={2}>
        <Grid item xs={12} s={6} md={6}>
          <Paper style={{ padding: "10px"}}>
            <h1 style={{ textAlign: "center" }}>Games</h1>
            <Link to="/games">
              <img
                src="https://images.pexels.com/photos/275033/pexels-photo-275033.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260"
                alt="game controller"
                className={classes.img}
              />
            </Link>
          </Paper>
        </Grid>
        <Grid item xs={12} s={6} md={6}>
          <Paper style={{ padding: "10px"}}>
            <h1 style={{ textAlign: "center" }}>Movie</h1>
            <Link to="/movies">
              <img
                src="https://images.pexels.com/photos/157543/pexels-photo-157543.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                alt="movie cassette"
                className={classes.img}
              />
            </Link>
          </Paper>
        </Grid>
      </Grid>
    </div>
  )
}