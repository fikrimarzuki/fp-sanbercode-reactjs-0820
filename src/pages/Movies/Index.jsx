import React, { useEffect } from "react";
import { Switch, Route, useRouteMatch } from "react-router-dom";
import { moviesActions } from "../../store";

import MovieList from "../../components/Movies/MovieList";
import MovieTable from "../../components/Movies/MovieTable";
import MovieReview from "../../components/Movies/MovieReview";
import MovieUpdate from "../../components/Movies/MovieUpdate";
import MovieCreate from "../../components/Movies/MovieCreate";

import { MovieContext } from "../../context/Index";

export default function Movies() {
  let { path } = useRouteMatch();
  const { setMovies } = React.useContext(MovieContext);

  useEffect(() => {
    async function fetchData() {
      const data = await moviesActions.fetchMovies();
      setMovies(data);
    }
    fetchData()
  }, [setMovies])

  return (
    <>
      <Switch>
        <Route exact path={path}>
          <MovieList />
        </Route>
        <Route path={`${path}/create`}>
          <MovieCreate />
        </Route>
        <Route path={`${path}/list`}>
          <MovieList />
        </Route>
        <Route path={`${path}/table`}>
          <MovieTable />
        </Route>
        <Route path={`${path}/edit/:id`}>
          <MovieUpdate />
        </Route>
        <Route path={`${path}/:id`}>
          <MovieReview />
        </Route>
      </Switch>
    </>
  )
}
