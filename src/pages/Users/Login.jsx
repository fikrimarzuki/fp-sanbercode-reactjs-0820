import React from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router";

import { AuthContext } from "../../context/Index";

import { usersActions } from "../../store/index";

import {
  Grid,
  Button,
  Link,
  FormControl,
  TextField,
  InputAdornment,
  IconButton,
  withStyles
} from '@material-ui/core';
import {
  Visibility,
  VisibilityOff
} from "@material-ui/icons";

const styling = theme => ({
  form: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: "20px",
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: "300px"
    },
    "& .MuiFormControl-root": {}
  },
  "btn-login": {
    width: "200px",
    margin: "20px auto"
  },
  "btn-go-to-register": {
    cursor: "pointer"
  }
})

class Login extends React.Component {
  constructor() {
    super()
    this.state = {
      email: "",
      password: "",
      showPassword: false,
      error: "",
      showError: false
    }
  }

  static contextType = AuthContext;

  static propTypes = {
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    classes: PropTypes.object.isRequired
  }

  handleChange = prop => event => {
    this.setState({ ...this.state, [prop]: event.target.value })
  }

  login = async event => {
    event.preventDefault();
    const data = await usersActions.signIn({ email: this.state.email, password: this.state.password })
    if (!data || data.error) {
      this.setState({ ...this.state, showError: true, error: data.error })
    } else {
      this.context.setUser(data.token);
      this.setState({ ...this.state, email: "", password: "", showError: false, error: "" })
      this.props.history.push("/");
    }
  }

  render() {
    const { history, classes } = this.props;
    const { error, showError } = this.state;

    return (
      <Grid
        container
        item
        xs={12}
        direction="column"
        alignItems="center"
        justify="center"
      >
        <div>
          <div style={{ textAlign: "center" }}>
            <h1 >LOGIN</h1>
            <p style={{ color: "red" }}>
              { showError ? `${error} check your input and try login again` : "" }
            </p>
          </div>
          <form className={classes.form} onSubmit={this.login}>
            <FormControl>
              <TextField
                error={false}
                required
                label="Email"
                onChange={this.handleChange("email")}
                placeholder="example@mail.com"
                multiline
                helperText={"Write your email here"}
                variant="outlined"
              />
              <TextField
                error={false}
                required
                label="Password"
                onChange={this.handleChange("password")}
                type={this.state.showPassword ? "text" : "password"}
                helperText={"Write your password here"}
                variant="outlined"
                InputProps={{
                  endAdornment: (<InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={e => this.setState({ ...this.state, showPassword: !this.state.showPassword})}
                      onMouseDown={event => event.preventDefault}
                    >
                      {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                  )
                }}
              />
              <Button className={classes["btn-login"]} variant="contained" size="medium" color="primary" type="submit">Login</Button>
            </FormControl>
          </form>
        </div>
        <Link className={classes["btn-go-to-register"]} onClick={() => history.push("/register")}>Register here</Link>
      </Grid>
    )
  }
}

export default withRouter(withStyles(styling)(Login));
