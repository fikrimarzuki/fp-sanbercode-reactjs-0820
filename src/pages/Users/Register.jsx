import React from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router";

import { AuthContext } from "../../context/Index";

import { usersActions } from "../../store/index";

import {
  Grid,
  Button,
  Link,
  FormControl,
  TextField,
  InputAdornment,
  IconButton,
  withStyles
} from '@material-ui/core';
import {
  Visibility,
  VisibilityOff
} from "@material-ui/icons";

const styling = theme => ({
  form: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: "20px",
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: "300px"
    },
    "& .MuiFormControl-root": {}
  },
  "btn-register": {
    width: "200px",
    margin: "20px auto"
  },
  "btn-go-to-login": {
    cursor: "pointer"
  }
})

class Register extends React.Component {
  constructor() {
    super()
    this.state = {
      name: "",
      email: "",
      password: "",
      showPassword: false,
      error: "",
      showError: {
        name: false,
        email: false,
        password: false
      }
    }
  }

  static contextType = AuthContext;

  static propTypes = {
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    classes: PropTypes.object.isRequired
  }

  handleChange = prop => event => {
    this.setState({ ...this.state, [prop]: event.target.value })
  }

  register = async event => {
    event.preventDefault();
    const data = await usersActions.signUp({ name: this.state.name, email: this.state.email, password: this.state.password })
    if (!data || data.error) {
      this.setState({ ...this.state, showError: true, error: data.error })
    } else {
      this.context.setUser(data.token);
      this.setState({ ...this.state, name: "", email: "", password: "", showError: false, error: "" })
      this.props.history.push("/");
    }
  }

  render() {
    const { history, classes } = this.props;
    const { error, showError } = this.state;

    return (
      <Grid
        container
        item
        xs={12}
        direction="column"
        alignItems="center"
        justify="center"
      >
        <div>
          <div style={{ textAlign: "center" }}>
            <h1 >Register</h1>
            <p style={{ color: "red" }}>
              { showError.name || showError.email || showError.password ? `${error} check your input and try login again` : "" }
            </p>
          </div>
          <form className={classes.form} onSubmit={this.register}>
            <FormControl>
              <TextField
                error={showError.name}
                required
                label="Name"
                onChange={this.handleChange("name")}
                placeholder="name"
                multiline
                helperText={"Write your name here"}
                variant="outlined"
              />
              <TextField
                error={showError.email}
                required
                label="Email"
                onChange={this.handleChange("email")}
                placeholder="example@mail.com"
                multiline
                helperText={"Write your email here"}
                variant="outlined"
              />
              <TextField
                error={showError.password}
                required
                label="Password"
                onChange={this.handleChange("password")}
                type={this.state.showPassword ? "text" : "password"}
                helperText={"Write your password here"}
                variant="outlined"
                InputProps={{
                  endAdornment: (<InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={e => this.setState({ ...this.state, showPassword: !this.state.showPassword})}
                      onMouseDown={event => event.preventDefault}
                    >
                      {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                  )
                }}
              />
              <Button className={classes["btn-register"]} variant="contained" size="medium" color="primary" type="submit">Register</Button>
            </FormControl>
          </form>
        </div>
        <Link className={classes["btn-go-to-login"]} onClick={() => history.push("/login")}>Login here</Link>
      </Grid>
    )
  }
}

export default withRouter(withStyles(styling)(Register));
