const sanberAPI = require("../config/sanberAPI");
const Cookies = require("js-cookie");

export default {
  fetchGames: async () => {
    try {
      const { data } = await sanberAPI.get("/data-game");
      return data;
    } catch(err) {
      console.log(err.response);
    }
  },
  fetchGame: async id => {
    try {
      const { data } = await sanberAPI.get(`/data-game/${id}`);
      return data;
    } catch(err) {
      console.log(err.response);
    }
  },
  createGame: async payload => {
    const headers = {
      Authorization: `Bearer ${Cookies.get("token")}`
    }
    try {
      const { data } = await sanberAPI.post("/data-game", payload, { headers });
      return data;
    } catch(err) {
      console.log(err.response);
    }
  },
  updateGame: async payload => {
    const headers = {
      Authorization: `Bearer ${Cookies.get("token")}`
    }
    try {
      const { data } = await sanberAPI.put(`/data-game/${payload.id}`, payload, { headers });
      return data;
    } catch(err) {
      console.log(err.response)
    }
  },
  deleteGame: async id => {
    const headers = {
      Authorization: `Bearer ${Cookies.get("token")}`
    }
    try {
      const { data } = await sanberAPI.delete(`/data-game/${id}`, { headers });
      return data;
    } catch(err) {
      console.log(err.response)
    }
  }
}