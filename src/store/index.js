import gamesActions from "./games";
import moviesActions from "./movies";
import usersActions from "./users";

export {
  gamesActions,
  moviesActions,
  usersActions
}