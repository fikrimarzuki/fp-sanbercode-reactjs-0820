const sanberAPI = require("../config/sanberAPI");
const Cookies = require("js-cookie");

export default {
  fetchMovies: async () => {
    try {
      const { data } = await sanberAPI.get("/data-movie");
      return data;
    } catch(err) {
      console.log(err.response);
    }
  },
  fetchMovie: async id => {
    try {
      const { data } = await sanberAPI.get(`/data-movie/${id}`);
      return data;
    } catch(err) {
      console.log(err.response);
    }
  },
  createMovie: async payload => {
    const headers = {
      Authorization: `Bearer ${Cookies.get("token")}`
    }
    try {
      const { data } = await sanberAPI.post("/data-movie", payload, { headers });
      return data;
    } catch(err) {
      console.log(err.response);
    }
  },
  updateMovie: async payload => {
    const headers = {
      Authorization: `Bearer ${Cookies.get("token")}`
    }
    try {
      const { data } = await sanberAPI.put(`/data-movie/${payload.id}`, payload, { headers });
      return data;
    } catch(err) {
      console.log(err.response);
    }
  },
  deleteMovie: async id => {
    const headers = {
      Authorization: `Bearer ${Cookies.get("token")}`
    }
    try {
      const { data } = await sanberAPI.delete(`/data-movie/${id}`, { headers });
      return data;
    } catch(err) {
      console.log(err.response);
    }
  }
}
