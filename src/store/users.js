const sanberAPI = require("../config/sanberAPI");
const Cookies = require("js-cookie");

export default {
  signUp: async payload => {
    try {
      const { data } = await sanberAPI.post("/register", payload);
      Cookies.set("token", data.token);
      return data;
    } catch(err) {
      if (err.response && err.response.data) {
        return { error: err.response.data };
      } else {
        return null;
      }
    }
  },
  signIn: async payload => {
    try {
      const { data } = await sanberAPI.post("/user-login", payload);
      Cookies.set("token", data.token);
      return data;
    } catch(err) {
      if (err.response && err.response.data) {
        return { error: err.response.data };
      } else {
        return null;
      }
    }
  },
  changePass: async payload => {
    const headers = {
      Authorization: `Bearer ${Cookies.get("token")}`
    }
    try {
      const { data } = await sanberAPI.post("/change-password", payload, { headers })
      return data;
    } catch(err) {
      if (err.response && err.response.data) {
        return { error: err.response.data };
      } else {
        return null;
      }
    }
  }
}
